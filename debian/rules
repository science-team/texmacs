#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk
export DH_VERBOSE=1

GUILE ?= /usr/bin/guile
MKDIR_P ?= /bin/mkdir -p
XVFB_RUN ?= /usr/bin/xvfb-run

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

export XDG_CACHE_HOME=$(CURDIR)/_XDG_CACHE_HOME
export TEXMACS_HOME_PATH=$(CURDIR)/_TEXMACS_HOME_PATH

DEB_GUILE_FALLBACK_PATH=$(shell XDG_CACHE_HOME=$(XDG_CACHE_HOME) $(GUILE) -c '(display %compile-fallback-path) (newline)')
DEB_GUILE_SITE_CCACHE_DIR=$(shell $(GUILE) -c '(display (%site-ccache-dir)) (newline)')
DEB_R_SITE_LIBRARY_DIR=/usr/lib/R/site-library/
export DEB_GS_VERSION=$(shell gs --version)

export VERSION_REVISION="Debian $(DEB_VERSION)"
export VERSION_STAMP="(Debian $(DEB_VERSION))"
export VERSION_COMMENT="Packaged by Debian ($(DEB_VERSION))"

export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

SOURCE_DATE_RSTAMP = $(shell date -u -d @$(SOURCE_DATE_EPOCH) +'%Y-%m-%d %H:%M:%S UTC')

default:
	@uscan --no-conf --dehs --report || true

%:
	dh $@

## passing datadir and libexecdir as option parameters allows
## to hard-code them (see TM_INSTALL in m/m/tm_install.m4).
override_dh_auto_configure:
	dh_auto_configure -- \
		--datadir=/usr/share \
		--libexecdir=/usr/libexec/$(DEB_HOST_MULTIARCH) \
		--with-guile2 --enable-guile2 \
		--with-imlib2 \
		--with-sqlite3

TEXMACS_PATH=$(CURDIR)/debian/tmp/usr/share/texmacs
TEXMACS_BIN_PATH=$(CURDIR)/debian/tmp/usr/libexec/$(DEB_HOST_MULTIARCH)/texmacs
TEXMACS_DOC_PATH=$(CURDIR)/debian/tmp/usr/share/doc/texmacs/documentation/
TEXMACS_SCRIPT=$(CURDIR)/debian/tmp/usr/bin/texmacs
DEB_TEXMACS_FALLBACK_PATH=$(DEB_GUILE_FALLBACK_PATH)/$(TEXMACS_PATH)
DEB_TEXMACS_SITE_CCACHE=$(CURDIR)/debian/tmp/$(DEB_GUILE_SITE_CCACHE_DIR)/texmacs

DEB_TEXMACS_SITE_CCACHE_COMPILED_BOOST=$(DEB_TEXMACS_SITE_CCACHE)/progs/kernel/boot/boot.go
DEB_GUILE_SITE_CCACHE_TEXMACS_COMPILED_BOOST=$(DEB_TEXMACS_SITE_CCACHE)/progs/usr/share/texmacs/progs/kernel/boot/boot.scm.go

TEXMACS=\
		XDG_CACHE_HOME=$(XDG_CACHE_HOME) \
		TEXMACS_HOME_PATH=$(TEXMACS_HOME_PATH) \
		TEXMACS_PATH=$(TEXMACS_PATH) \
		TEXMACS_BIN_PATH=$(TEXMACS_BIN_PATH) \
		TEXMACS_DOC_PATH=$(TEXMACS_DOC_PATH) \
	$(XVFB_RUN) $(TEXMACS_SCRIPT) -d

R_CMD=\
	R CMD

DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_SCHEME=$(CURDIR)/debian/adhoc/site-ccache.scm
DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_COMPILED_SCHEME=$(DEB_GUILE_FALLBACK_PATH)/$(DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_SCHEME).go
$(DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_COMPILED_SCHEME): $(DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_SCHEME)
	$(MKDIR_P) $(XDG_CACHE_HOME)
	$(MKDIR_P) $(TEXMACS_HOME_PATH)
	$(MKDIR_P) $(DEB_TEXMACS_SITE_CCACHE)
	$(TEXMACS) -x '(load "$(DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_SCHEME)")'
execute_after_dh_auto_install_go: $(DEB_GUILE_SITE_CCACHE_TEXMACS_ADHOC_COMPILED_SCHEME)
	$(eval logof:=$(shell find $(DEB_TEXMACS_FALLBACK_PATH) -type f -name '*.scm.go' -printf '%P '))
	$(foreach gof, $(logof),\
		$(MKDIR_P) $(DEB_TEXMACS_SITE_CCACHE)/$(dir $(gof)) ;$(NEWLINE)\
		cp -p -v $(DEB_TEXMACS_FALLBACK_PATH)/$(gof) $(DEB_TEXMACS_SITE_CCACHE)/$(gof:.scm.go=.go) ;$(NEWLINE)\
		)

DEB_R_SITE_SRC=$(CURDIR)/debian/tmp/DEBIAN/SRC/R/
DEB_R_SITE_LIBRARY=$(CURDIR)/debian/texmacs-base/$(DEB_R_SITE_LIBRARY_DIR)
DEB_TEXMACS_PLUGINS_RDIR=$(CURDIR)/debian/tmp/usr/share/texmacs/plugins/r/r
execute_after_dh_auto_install_Rplg:
	$(MKDIR_P) $(DEB_R_SITE_SRC)
	$(MKDIR_P) $(DEB_R_SITE_LIBRARY)
	{ cd $(DEB_R_SITE_SRC) && \
		$(R_CMD) build --compression=none --user='Debian; $(DEB_VERSION)' $(DEB_TEXMACS_PLUGINS_RDIR)/TeXmacs ; \
		}
	rm -rf $(DEB_TEXMACS_PLUGINS_RDIR)/TeXmacs
	rmdir $(DEB_TEXMACS_PLUGINS_RDIR)

DEB_TEXMACS_INSTALLER_PLUGINS_PATH=$(CURDIR)/debian/tmp/usr/share/doc/texmacs/installers/plugins
execute_after_dh_auto_install: execute_after_dh_auto_install_go execute_after_dh_auto_install_Rplg
	$(MKDIR_P) $(DEB_TEXMACS_INSTALLER_PLUGINS_PATH)
	$(MKDIR_P) $(dir $(DEB_GUILE_SITE_CCACHE_TEXMACS_COMPILED_BOOST))
	ln -rs $(DEB_TEXMACS_SITE_CCACHE_COMPILED_BOOST) $(DEB_GUILE_SITE_CCACHE_TEXMACS_COMPILED_BOOST)
	$(eval lopigof:=$(shell find $(DEB_TEXMACS_SITE_CCACHE)/plugins -type f -name '*.go' -printf '%P '))
	ln -s -t $(DEB_TEXMACS_SITE_CCACHE)/plugins $(lopigof)
	$(R_CMD) INSTALL \
			--built-timestamp='$(SOURCE_DATE_RSTAMP)' \
			--library=$(DEB_R_SITE_LIBRARY) \
		$(DEB_R_SITE_SRC)/TeXmacs_*.tar
	sed -i \
			-e 's@^Packaged: .* UTC;@Packaged: $(SOURCE_DATE_RSTAMP);@' \
		$(DEB_R_SITE_LIBRARY)/TeXmacs/DESCRIPTION
	mv -v $(TEXMACS_BIN_PATH)/bin/tm_*_install $(DEB_TEXMACS_INSTALLER_PLUGINS_PATH)

DEB_TEXMACS_THEMESDIR=$(CURDIR)/debian/texmacs-base/usr/share/texmacs/packages/themes/
DEB_TEXMACS_DOC_EXTMDIR=$(CURDIR)/debian/texmacs-doc/usr/share/doc/texmacs/examples/texts/
DEB_SEDSUBST_TMFS_ARTWORK='s@tmfs://artwork/@/usr/share/texmacs/misc/@g'
DEB_SEDSUBST_GRDPRNT='s@\.\./\.\./misc/@/usr/share/texmacs/misc/@g'
execute_after_dh_installexamples-indep:
	$(eval lowts:=$(shell grep -l -r '|tmfs://artwork/' $(DEB_TEXMACS_THEMESDIR) ))
	$(foreach wts,$(lowts), sed -i -e $(DEB_SEDSUBST_TMFS_ARTWORK) $(wts) ;$(NEWLINE))
	sed -i -e $(DEB_SEDSUBST_GRDPRNT) $(DEB_TEXMACS_DOC_EXTMDIR)/torture-images.tm
	sed -e $(DEB_SEDSUBST_TMFS_ARTWORK) \
				$(DEB_TEXMACS_DOC_EXTMDIR)/torture-images.tm \
		> $(DEB_TEXMACS_DOC_EXTMDIR)/torture-images-local.tm

DEB_TEXMACS_PATH=/usr/share/texmacs
DEB_TEXMACS_DOC_DOCUMENTATIONPATH=/usr/share/doc/texmacs/documentation
DEB_TEXMACS_DOC_DOCUMENTATIONDIR=$(CURDIR)/debian/texmacs-doc/$(DEB_TEXMACS_DOC_DOCUMENTATIONPATH)
execute_after_dh_link-indep:
	$(eval lopif:=$(shell find $(DEB_TEXMACS_DOC_DOCUMENTATIONDIR)/plugins -maxdepth 1 -type d -printf '%P '))
	$(foreach pif,$(lopif),\
		dh_link -p texmacs-doc \
			$(DEB_TEXMACS_DOC_DOCUMENTATIONPATH)/plugins/$(pif) $(DEB_TEXMACS_PATH)/plugins/$(pif)/doc ;$(NEWLINE)\
		)

override_dh_compress-indep:
	dh_compress -Xexamples -X.tm -X.bib -Xgiac-demo -Xtm-page.ps

execute_before_dh_installdocs-indep:
	cp -p TeXmacs/README README.TeXmacs
	$(TEXMACS) --build-manual texmacs-scheme-manual.en
	sleep 2
	$(TEXMACS) --build-manual texmacs-source-manual.en
	sleep 2
	$(TEXMACS) --build-manual texmacs-user-manual.en
	sleep 2
	$(TEXMACS) --build-manual texmacs-reference-manual.en

override_dh_installdocs:
	dh_installdocs -ptexmacs-base  --doc-main-package=texmacs
	dh_installdocs -ptexmacs-texmf --doc-main-package=texmacs
	dh_installdocs -ptexmacs-doc   --doc-main-package=texmacs
	dh_installdocs --remaining-packages

override_dh_installexamples:
	dh_installexamples -ptexmacs-base  --doc-main-package=texmacs
	dh_installexamples -ptexmacs-doc   --doc-main-package=texmacs
	dh_installexamples --remaining-packages

override_dh_missing:
	dh_missing --list-missing

override_dh_dwz-arch:
	dh_dwz -a -X.go

override_dh_shlibdeps-arch:
	dh_shlibdeps -a -X.go

get-info:
	@echo 'SOURCE_DATE_EPOCH          >$(SOURCE_DATE_EPOCH)<'
	@echo 'SOURCE_DATE_RSTAMP         >$(SOURCE_DATE_RSTAMP)<'
	@echo 'DEB_VERSION:               >$(DEB_VERSION)<'
	@echo 'VERSION_STAMP:             >$(VERSION_STAMP)<'
	@echo 'VERSION_COMMENT:           >$(VERSION_COMMENT)<'
	@echo 'XDG_CACHE_HOME:            >$(XDG_CACHE_HOME)<'
	@echo 'DEB_GUILE_FALLBACK_PATH:   >$(DEB_GUILE_FALLBACK_PATH)<'
	@echo 'DEB_GUILE_SITE_CCACHE_DIR: >$(DEB_GUILE_SITE_CCACHE_DIR)<'
	@echo 'DEB_R_SITE_LIBRARY_DIR:    >$(DEB_R_SITE_LIBRARY_DIR)<'
	@echo 'DEB_GS_VERSION:            >$(DEB_GS_VERSION)<'

define NEWLINE


endef
