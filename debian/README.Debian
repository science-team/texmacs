Debian specific system-wide setup
---------------------------------

TeXmacs presents two layers: the user layer, TEXMACS_HOME_PATH, and the
system-wide layer, TEXMACS_PATH. As expected, Debian install the latter
under /usr, that is to say that TEXMACS_PATH is set to "/usr/share/texmacs"
by default. However, for allowing superusers to install system-wide TeXmacs
material without interfering with the TeXmacs material installed by Debian,
Debian introduces a local, intermediate system-wide layer under /usr/local,
TEXMACS_LOCAL_PATH. As expected, TEXMACS_LOCAL_PATH is set by default to
"/usr/local/share/texmacs". Though TEXMACS_HOME_PATH is assumed to be set
to "${HOME}/.TeXmacs", users are free to set it as they wish. The order of
priority is TEXMACS_HOME_PATH<TEXMACS_LOCAL_PATH<TEXMACS_PATH as expected.
The TEXMACS_LOCAL_PATH and TEXMACS_PATH environment variables are actually
set up, among others, in the shell script "/usr/bin/texmacs".


Official Debian balls vs upstream Debian ball
---------------------------------------------

The upstream TeXmacs Team provides its own Debian ball. However, their
Debian ball does not follows the Debian Standards. They acknowledge this
by installing TeXmacs under /usr/local. Their TEXMACS_PATH is set to
"/usr/local/share/TeXmacs" by default. Note the majuscules. So their
installed material may not overwrite neither the material installed by
the official Debian balls nor the local system-wide TeXmacs material as
assumed above. And vice-versa. However, the two distributions may not
coexist well in details. For this reason, the official Debian balls
forcedly replaces (Breaks and Replaces) the upstream Debian ball
while it provides a `dependency package' (read `dummy') named texmacs
that sets up a standard TeXmacs installation.


 -- Jerome Benoit <calculus@rezozer.net>  Sat, 07 Sep 2024 17:41:17 +0000
